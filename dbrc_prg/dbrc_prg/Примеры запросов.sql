 DECLARE @i int = 0, @length int = 100; 
while  @i < @length
begin
INSERT INTO [dbo].[Goods] ([barcode],[product_name],[code_country],[net_weight],[gross_weight],[expiration_date],[storage_conditions],[count_layer],[count_length],[count_width],[login_usr],[modified_date])
VALUES
(
cast(cast((RAND()*1000000000000) as bigint) as nchar(13)),
cast(newid() as  nchar(250)),cast((RAND()*10) as int),
cast((RAND()*10) as int),cast((RAND()*10) as int),
cast((RAND()*10) as int),cast(newid() as  nchar(250)),
cast((RAND()*10) as int),cast((RAND()*10) as int),
cast((RAND()*10) as int),SYSTEM_USER,GETDATE()
);

set @i += 1;
end;

-- ����� ���������� ������
select * from test_dbrc.dbo.goods;
--����� ������� ��� ������ (������)
TRUNCATE TABLE   [test_dbrc].dbo.goods
--���������� ���������� ����� ������ 
 print DATEDIFF(day, '2018-05-10 01:04:05.537', GETDATE()) 
 
   --�������� �������
CREATE TABLE Goods	
(					
code_product						int				PRIMARY KEY		NOT NULL		IDENTITY (1,1)				,
barcode								nchar(13)						NULL										,
product_name						nchar(250)						NOT NULL									,
code_country						int								NOT NULL		default 0					,
net_weight							int								NULL			check(net_weight >0)		,
gross_weight						int								NULL			check(gross_weight>0)		,
expiration_date						int								NULL			check(expiration_date>0)	,
storage_conditions					nchar(250)						NULL										,
count_layer							int								NULL										,
count_length						int								NULL										,
count_width							int								NULL										,
login_usr							nchar(20)						NULL										,
modified_date						date							NULL				
);

--*****************************************************************************--

-- ������ � ����������� � ����� � ��������� ������
DECLARE @name NVARCHAR(20), @age INT;
SET @name='Tom';
SET @age = 18;
PRINT 'Name: ' + @name;
PRINT 'Age: ' + CONVERT(CHAR, @age);


--*****************************************************************************--

--��������� ���������� ������������, ��� ����������� ������������� ��������������
sp_configure 'show advanced options', 1;  
RECONFIGURE;
GO 
sp_configure 'Ad Hoc Distributed Queries', 1;  
RECONFIGURE;  
GO  

--������ ��� ��������� ��������-�������� ������
insert into Table_test 
select * from   opendatasource 
('sqlncli','Data Source= asuspc;user id = sa; pwd=a1b2c3d4').test0.[dbo].[Table_test];

--*****************************************************************************--
-- 
CREATE FUNCTION dbo.CombinedSalesInfo ()
RETURNS @return_variable TABLE
    (
     SalesPersonID INT,
     ShippingCity NVARCHAR(30),
     OrderDate DATETIME,
     PurchaseOrderNumber dbo.OrderNumber,
     AccountNumber dbo.AccountNumber,
     OrderQty SMALLINT,
     UnitPrice MONEY
    )
AS
    BEGIN;
        INSERT  INTO @return_variable
                (SalesPersonId,
                 ShippingCity,
                 OrderDate,
                 PurchaseOrderNumber,
                 AccountNumber,
                 OrderQty,
                 UnitPrice
                )
                SELECT  si.SalesPersonID,
                        si.ShippingCity,
                        si.OrderDate,
                        si.PurchaseOrderNumber,
                        si.AccountNumber,
                        sd.OrderQty,
                        sd.UnitPrice
                FROM    dbo.SalesInfo() AS si
                        JOIN dbo.SalesDetails() AS sd
                        ON si.SalesOrderID = sd.SalesOrderID ;
        RETURN ;
    END ;
GO



--*****************************************************************************--


--�������� ��������

--������ �����������, ��� �������������� ����� MySite.ru ����� ������� �� ������� ������������� ������ ����� ���, ��� �� ������� �� ���� ����� ����, � ��������� �� �����������. �� ���������� ������� ��������� ��������� ��������, ����� ������������������ ������ ������������ DBMail � SQL, ������� ������� � �����. ��� ���������� ������ ������� ��� ��������� �������:
--������ �� ������ ���������� ���, ��� ������ ��� �����������, ��� �� ������ �������� ������. ��� ��� ������� ��������� ����� ���������� ������������ ��� ������ ���������� SQL-������� � �����.
--������ ������ ���� ��������� � ������� HTML.
--������ ������ ����������� � ���������� � 3 �������, ����� �� ����������� ��� SMTP-������.
--�������� ������ �������� �� ����� ����� ������� SQL-�������, �, ������, � 3 ���� ����.

--https://habr.com/post/179819/



-- ���������� �������� ���� ������ mysite
USE mysite
GO
-- ������� ����������: ������������� ������������, ���,
-- ���� ���������� ����� �� ����, �������� �����
	DECLARE @user_id int, @user_name nvarchar(255), @last_login_date smalldatetime,
		@email_address varchar(255);
-- ���������� @body ����� ��������� ����� ������ � ������� HTML
	DECLARE @body nvarchar(MAX);
-- ���������� @no_mail ����� ���������� �������������, ������������ �� ��������
	DECLARE @no_mail int;
-- ������� ������ ���� ������, ������ ����������� ������ ���� ���������
-- ��� ������� �������� ����� ������ �������� ������� FAST_FORWARD
	DECLARE users CURSOR LOCAL FAST_FORWARD FOR
		SELECT id, name, last_login_date, email_address
		FROM users
		WHERE user_role IN (3,4,5)
			AND account_state = 2
			AND email_address IS NOT NULL
			AND DATEDIFF(day, last_login_date, GETDATE()) > 365
		ORDER BY id
-- ����� ������ �������� �������� �� ������, � � 3 ���� ����
	WAITFOR TIME '03:00:00'
-- ��������� ������ users
	OPEN users
-- ������� ������ ��� ������� �� �������
	FETCH NEXT FROM users INTO @user_id, @user_name, @last_login_date, @email_address
-- �������� ���� �� ����� �������
WHILE @@FETCH_STATUS = 0
BEGIN
	-- ���������, �� ��������� �� ������������ �� ��������
		SET @no_mail = (SELECT id FROM users WHERE id = @user_id AND allow_mail = 0)
	-- ���� ���������, �� �������� � ���������� ������������
		IF @no_mail IS NOT NULL
			BEGIN
				PRINT N'������������ ' + @user_name + N' ��������� �� ��������.'
				FETCH NEXT FROM users INTO @user_id, @user_name, @last_login_date, @email_address
				CONTINUE
			END

		PRINT N'�������� ������ ��� ' + @email_address + N' ...'

	-- ���������� ����� ������
		SET @body = N'
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>�����������</title>
</head>
<body>
<p><img style="float:right;" src="http://mysite.ru/images/logo.png"/></p>
<p>������������, ' + @user_name + N'!</p>
<p>���������� ��� ����� �������� ���� MySite.ru. � ��� ��������� ����� ����� ���������� �������,
���������� ����������, � �� ��������� ����� ������ ��� ��������� �������� ����� ������ � ������.</p>
<p>������ �������!</p>
<p>� ���������, ������������� ����� MySite.ru</p>
</body>
</html>';

	-- �������� ������
		EXEC msdb.dbo.sp_send_dbmail
			@recipients = @email_address,
			@subject = N'���������� ��� ����� �������� ���� MySite.ru',
			@body = @body,
		-- ������ ������ ����� ���� ���� 'HTML', ���� 'TEXT'
			@body_format = 'HTML',
		-- ��� ������������� � ������ ����� ���������� ����
			--@file_attachments ='C:\attachment.jpg',
		-- ����� ����� ������� ����� ��� ����������� �����
			--@copy_recipients ='me@gmail.com',
		-- "Blind copy" ��� "carbon copy" - ��� �������� ����� ������,
		-- ���������� ������� �� ����� ������� ����������� ������ �����
			--@blind_copy_recipients ='me2@gmail.com',
		-- ������ ��������� ����� ������� �������������� �������� ��������
			@profile_name = 'MySite Admin Mailer';

	-- ��������� �������� � 3 ������� ����� ��������, ����� �� ����������� SMTP-������
		WAITFOR DELAY '00:00:03';
	-- � ����� ����� ����� ��������� ������ �� �������
		FETCH NEXT FROM users INTO @user_id, @user_name, @last_login_date, @email_address
END
-- ��������� ������ ���� ������
CLOSE users
GO

--*****************************************************************************--



GO
DECLARE @id_global_test int;
DECLARE @data_n DATETIME;
DECLARE @data_k DATETIME;
DECLARE @i INT;
declare @coun_str int;
DECLARE @id_test INT;
DECLARE @id_iter INT;
set @id_global_test=(select max(id) from dbo.[statistics])+1;
set @id_test=1;
set @id_iter=0;
set @coun_str = 1000000;
 --���� ������
	WHILE @id_iter<10 BEGIN
		SET @data_n=getdate();
		SET @i=0;
 			WHILE @i<@coun_str
			BEGIN
				INSERT INTO [dbo].[Table_test] (text ) VALUES ('Test string');
				SET @i=@i+1;
			END
			SET @id_iter=@id_iter+1;
		SET @data_k=getdate();
		insert into [dbo].[statistics] (id,id_test,id_iter, coun_str,[time]) values (@id_global_test,@id_test,@id_iter, @coun_str,(@data_k - @data_n));
		waitfor delay '00:05:00'		
	end

 --*****************************************************************************--

print @@ROWCOUNT � ������ ���������� �������, ������������ ���������� ��������;
print @@ERROR � ���������� ��� ������ ��� ��������� �������;
print @@SERVERNAME - ��� ���������� SQL �������;
print @@VERSION - ����� ������ SQL Server;
print @@IDENTITY - ��������� �������� ��������, ������������ � �������� ������� (insert).
--*****************************************************************************--


--RETURN
--������ ������� ������ ��� ������������ ������ �� ������� ��� ���������. 
--RETURN ����� �������������� � ����� ����� ��� ������ �� ���������, ������ 
--��� ����� ����������. ��� ��� ���� ����� ���� ������� �� �����������.


--*****************************************************************************--
BEGIN TRY
DECLARE @TestVar1 int = 10, @TestVar2 int = 0, @result int
SET @result = @TestVar1 / @TestVar2
END TRY
BEGIN CATCH
	SELECT
	ERROR_NUMBER() as [����� ������],
	ERROR_MESSAGE() as [�������� ������]
END CATCH





