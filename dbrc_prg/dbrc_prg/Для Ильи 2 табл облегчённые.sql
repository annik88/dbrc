  
  --������� �������
  /*
	  goods								�����	
code_product		int	PRIMARY KEY	NOT NULL		IDENTITY (1,1)			,	��� ������	
barcode				nchar(13)		NULL									,	�� ������	
product_name		nchar(250)		NOT NULL								,	�������� ������	
code_country		int				NOT NULL		default 0				,	��� ������ ������������	���������� ������� ���������� �� ��������
net_weight			int				NULL			check(net_weght>0)		,	��� �����	��
gross_weight		int				NULL			check(gross_weght>0)	,	��� ������	��
expiration_date		int				NULL			check(gross_weght>0)	,	���� ��������	�����
storage_conditions	nchar(250)		NULL									,	������� ��������	
count_layer			int				NULL									,	���-�� ���� �� �������	
count_length		int				NULL									,	���-�� ���� � �����	
count_width			int				NULL									,	���-�� ���� � ������	
login_usr			nchar(20)		NULL									,	������������	������������ ������� ������� 
modified_date		date			NULL									);	���� ���������	
 
  */
  --�����������
  CREATE TABLE Goods	
(					
code_product						int				PRIMARY KEY		NOT NULL		IDENTITY (1,1)				,
barcode								nchar(13)						NULL										,
product_name						nchar(250)						NOT NULL									,
code_country						int								NOT NULL		default 0					,
net_weight							int								NULL			check(net_weight >0)		,
gross_weight						int								NULL			check(gross_weight>0)		,
expiration_date						int								NULL			check(expiration_date>0)	,
storage_conditions					nchar(250)						NULL										,
count_layer							int								NULL										,
count_length						int								NULL										,
count_width							int								NULL										,
login_usr							nchar(20)						NULL										,
modified_date						date							NULL				
);
-- ��� �����������
CREATE TABLE Goods	
(					
code_product						int				PRIMARY KEY		NOT NULL		IDENTITY (1,1)				,
barcode								nchar(13)						NULL										,
product_name						nchar(250)						NOT NULL									,
code_country						int								NOT NULL									,
net_weight							int								NULL										,
gross_weight						int								NULL										,
expiration_date						int								NULL										,
storage_conditions					nchar(250)						NULL										,
count_layer							int								NULL										,
count_length						int								NULL										,
count_width							int								NULL										,
login_usr							nchar(20)						NULL										,
modified_date						date							NULL				
);

/*
   Storage_location								����� ��������(������)		
code_cell			int	PRIMARY KEY	NOT NULL			IDENTITY (1,1)					��� ������		
num_row				int				NOT NULL											���		
num_section			int				NOT NULL											������		
num_level			int				NOT NULL											����		
num_pallet			int				NOT NULL											������		
address_cell		nchar(250)		NULL				CHECK (address_cell !='')		����� ������		
height_cell			int				NOT NULL			CHECK (height_cell >0)			������ ������							��
code_pallet			int				NULL					,							��� �������		
max_weight_in_cell	int				NOT NULL			CHECK (max_weight_in_cell >0)	����������� ���������� ��� � ������		��
code_status_cell	int				NOT NULL			default 3						��� ������� ������		
login_usr			nchar(20)		NOT NULL											����� ������������ ����������� ����		
modified_date		date			NOT NULL											���� ���������		
 
*/

--� �������������
CREATE TABLE Storage_location			
(				
code_cell							int				PRIMARY KEY		NOT NULL			IDENTITY (1,1)			,
num_row								int								NOT NULL									,
num_section							int								NOT NULL									,
num_level							int								NOT NULL									,
num_pallet							int								NOT NULL									,
address_cell						nchar(250)						NULL				CHECK (address_cell !='')	,
height_cell							int								NOT NULL			CHECK (height_cell >0)	,
code_pallet							int								NULL					,
max_weight_in_cell					int								NOT NULL			CHECK (max_weight_in_cell >0)	,
code_status_cell					int								NOT NULL			default 3		,
login_usr							nchar(20)						NOT NULL 				,
modified_date						datetime						NOT NULL 				
);

-- ��� �����������
CREATE TABLE Storage_location			
(				
code_cell							int				PRIMARY KEY		NOT NULL			IDENTITY (1,1)			,
num_row								int								NOT NULL									,
num_section							int								NOT NULL									,
num_level							int								NOT NULL									,
num_pallet							int								NOT NULL									,
address_cell						nchar(250)						NULL										,
height_cell							int								NOT NULL									,
code_pallet							int								NULL										,
max_weight_in_cell					int								NOT NULL									,
code_status_cell					int								NOT NULL									,
login_usr							nchar(20)						NOT NULL 									,
modified_date						datetime						NOT NULL 				
);