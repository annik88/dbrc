																														 -- ��������� ������ ���� ��� �������� � ������������ �����������
USE [test_dbrc]
GO	


--������� ������
declare @num_row			int = 1;		--	���������� �����(����� ����, ���� ��� ����)
declare @num_section		int = 3;		--	���������� ������
declare @num_level			int = 2;		--	���������� ������
declare @num_cell			int = 2;		--	���������� ����� � ������
declare @max_weight_in_cell int = 200000;	--	������������ ��� � ������
declare @height_cell		int = 1000	;	-- ������ � ������
--��������� ������������ � ��������������� ����
declare @address_cell nchar(250);	--����� ������	�1*�1*�1*�1
	 
--declare @temp_num_row			int = 0;		--	���
declare @temp_num_section		int = 0;		--	������
declare @temp_num_level			int = 0;		--	����
declare @temp_num_cell			int = 0;		--	����� ������   

while @temp_num_section<@num_section
	begin
	while @temp_num_level<@num_level
	begin
		while @temp_num_cell<@num_cell
			begin
					set @address_cell = '�'+cast(@num_row as varchar)+'*'+'�'+cast(@temp_num_section as varchar)+'*'+'�'+cast(@temp_num_level as varchar)+'*'+'�'+cast(@temp_num_cell as varchar);
					INSERT INTO [test_dbrc].[dbo].[Storage_location]
								([num_row]
								 ,[num_section]
								 ,[num_level]
								 ,[num_pallet]
								 ,[address_cell]
								 ,[height_cell]
								 --,[code_pallet]
								 ,[max_weight_in_cell]
								 --,[code_status_cell]
								 ,[login_usr]
								 ,[modified_date])
							VALUES
								(@num_row
								,@temp_num_section
								,@temp_num_level
								,@temp_num_cell
								,@address_cell
								,@height_cell
								,@max_weight_in_cell
								--,<code_status_cell, int,>
								,SYSTEM_USER								--print SYSTEM_USER
								,getdate()								--select getdate()
								);	

				set @temp_num_cell +=1;
			end;
			set @temp_num_cell =0;
			set @temp_num_level += 1;
		end;
		set @temp_num_level =0;
		set @temp_num_section +=1;
	end;
GO
/*
select * from test_dbrc.dbo.[Storage_location]

truncate table 	   test_dbrc.dbo.[Storage_location];
*/



