-- ������ 0.0.1
use test_dbrc
CREATE TABLE Goods	
(					
code_product						int				PRIMARY KEY		NOT NULL		IDENTITY (100000,1)				,
barcode								nchar(13)						NULL										,
product_name						nchar(250)						NOT NULL									,
code_country						int								NOT NULL		default 0					,
net_weight							int								NULL			check(net_weight >0)		,
gross_weight						int								NULL			check(gross_weight>0)		,
expiration_date						int								NULL			check(expiration_date>0)	,
storage_conditions					nchar(250)						NULL										,
count_layer							int								NULL										,
count_length						int								NULL										,
count_width							int								NULL										,
login_usr							nchar(20)						NULL										,
modified_date						date							NULL				
);
					
							
CREATE TABLE Pallets
(
code_pallet							int				PRIMARY KEY		NOT NULL	IDENTITY (1,1)					,
code_type_pallet					int								NULL										,
code_product						int								NULL										,
code_status_pallet					int								NOT NULL	default 3		
);
							
							
CREATE TABLE Status_pallet		
(					
code_status_pallet					int				PRIMARY KEY		NOT NULL									,
name_status							nchar(250)
);
							
CREATE TABLE Status_cell		
(				
code_status_cell					int				PRIMARY KEY		NOT NULL									,
name_status							nchar(250)
);
							
CREATE TABLE Type_pallet	
(						
code_type_pallet					int				PRIMARY KEY		NOT NULL			IDENTITY (100,1)					,
name_type_pallet					nchar(250)						NOT NULL			CHECK (name_type_pallet !='')	,
length_pallet						int								NOT NULL			CHECK (length_pallet >0)		,
widtht_pallet						int								NOT NULL			CHECK (widtht_pallet >0)		,
height_pallet						int								NOT NULL			CHECK (height_pallet >0)		,
weight_pallet						int								NOT NULL			CHECK (weight_pallet >0)		,
max_weight_on_pallet				int								NOT NULL			CHECK (max_weight_on_pallet >0)	
);
							
CREATE TABLE Allow_type_pallet	
(						
code_cell							int								NOT NULL											,
code_type_pallet					int								NOT NULL											,
code_status_allow_cell				int								NOT NULL			default 2						,
PRIMARY KEY(code_cell,code_type_pallet)							
);
CREATE TABLE 	Status_allow_cell
(				
code_status_allow_cell				int				PRIMARY KEY		NOT NULL											,
name_status							nchar(250)						NOT NULL				
);
							
CREATE TABLE Storage_location			
(				
code_cell							int				PRIMARY KEY		NOT NULL			IDENTITY (10000,1)			,
num_row								int								NOT NULL									,
num_section							int								NOT NULL									,
num_level							int								NOT NULL									,
num_pallet							int								NOT NULL									,
address_cell						nchar(250)						NULL				CHECK (address_cell !='')	,
height_cell							int								NOT NULL			CHECK (height_cell >0)	,
code_pallet							int								NULL					,
max_weight_in_cell					int								NOT NULL			CHECK (max_weight_in_cell >0)	,
code_status_cell					int								NOT NULL			default 3		,
login_usr							nchar(20)						NOT NULL 				,
modified_date						datetime						NOT NULL 				
);
go

--������ 0.0.2
use test_dbrc

--alter table [dbo].[Storage_location] drop column num_pallet;
--alter table [dbo].[Storage_location] alter column num_cell		int	NOT NULL ;
--alter table [dbo].[Storage_location] alter column num_row		int	NOT NULL ;
--alter table [dbo].[Storage_location] alter column num_section	int	NOT NULL ;
--alter table [dbo].[Storage_location] alter column num_level		int	NOT NULL ;
--alter table [dbo].[Storage_location] alter column login_usr		nchar(20)	NOT NULL ;
--alter table [dbo].[Storage_location] alter column modified_date	datetime	NOT NULL ;
			

-----------------------------------------------------------------------------------------
--select getdate()

--create table t1( nchar_row nchar(2) null)

--declare @i ;
--insert t1 values(convert(nchar, 2));

--select * from t1

